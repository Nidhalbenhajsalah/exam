
import Domaines from './pages/domaines';
import { BrowserRouter as Router, Routes, Route, Link } from 'react-router-dom';

function App() {
  return (
    <div className="App">
      <Router>
        <Routes>
          <Route path="/" element={<Domaines />} />

        </Routes>
      </Router>
    </div>
  );
}

export default App;
