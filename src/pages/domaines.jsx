import React from "react";
import Box from '@mui/material/Box';
import { Typography } from "@mui/material";
import Paper from '@mui/material/Paper';
import './domaines.css';
import { useState, useEffect } from "react";
import axios from 'axios';
import SingleDomain from "../components/singleDomain";



const Domaines = () => {
    const [chapters, setChapters] = useState([]);
    const [domains, setDomains] = useState([]);




    useEffect(() => {
        const url = "http://localhost:3000/chapters";
        const getChapters = async () => {
            const response = await axios.get(url);
            setChapters(response.data);
        }
        getChapters();
    }, [chapters]);

    //returns an array of unique domain values
    useEffect(() => {
        const arrayOfDomains = chapters.map(chapter => chapter.domain);
        setDomains([...new Set(arrayOfDomains)]);
    }, [chapters]);




    return (
        <Box>

            <Paper className="paper" elevation={3}>
                <Typography
                    variant="h2"
                    gutterBottom
                    align="center"
                    padding={2}
                    marginBottom={5}
                    color="primary"
                >
                    Test de connaissances</Typography>
            </Paper>
            <Paper className="paper-nb">
                <Typography
                    variant="h6"
                    gutterBottom
                    align='center'
                    color='primary'
                >
                    Attention: En ouvrant un module vous déclencher la minuterie, vous avez 10 minutes pour chaque module pour répondre aux questions
                </Typography>

            </Paper>

            <Box className="container">

                <Box sx={{ width: '100%', maxWidth: '80%', bgcolor: 'background.paper', margin: 'auto' }}>
                    {domains.map((domain, index) => (
                        <SingleDomain key={index} domain={domain} chapters={chapters} />
                    ))}
                </Box>
            </Box>
        </Box >
    );
}

export default Domaines;