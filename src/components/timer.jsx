import React, { useState, useEffect, useRef } from "react";
import Button from '@mui/material/Button';
import './timer.css'


const Timer = ({ whenTimeIsOver }) => {
    const intervalRef = useRef(null);
    const [timer, setTimer] = useState('00:30');




    const getTimeRemaining = (endTime) => {
        const total = Date.parse(endTime) - Date.parse(new Date());
        const seconds = Math.floor((total / 1000) % 60);
        const minutes = Math.floor((total / 1000 / 60) % 60);

        return {
            total, minutes, seconds
        };
    }

    const startTimer = (deadline) => {
        let { total, minutes, seconds } = getTimeRemaining(deadline);
        if (total >= 0) {
            setTimer(
                (minutes > 9 ? minutes : '0' + minutes) + ':' +
                (seconds > 9 ? seconds : '0' + seconds)
            )
        }
        else {
            clearInterval(intervalRef.current)
        }
    }

    const clearTimer = (endtime) => {
        setTimer("00:30");
        if (intervalRef.current) {
            clearInterval(intervalRef.current);
        }
        const id = setInterval(() => startTimer(endtime), 1000);
        intervalRef.current = id;
    }

    const getDeadline = () => {
        let deadline = new Date();
        deadline.setSeconds(deadline.getSeconds() + 30);
        return deadline;
    }

    useEffect(() => {
        clearTimer(getDeadline());
        return () => {
            if (intervalRef.current) {
                clearInterval(intervalRef.current);
            }
        }
    }, []);

    useEffect(() => {
        if (timer === '00:00') {
            whenTimeIsOver()
        }
        return
    }, [timer]);

    const handleClick = () => {
        setTimer("00:00");
    }

    return (
        <div>
            <span>Il vous reste: {timer}</span>
            <Button onClick={handleClick}>Soumettre</Button>

        </div>
    )

}

export default Timer;