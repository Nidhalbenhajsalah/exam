import React from "react";
import Box from '@mui/material/Box';

import { Typography } from "@mui/material";
import Paper from '@mui/material/Paper';
import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import { useState, useEffect } from "react";
import axios from "axios";
import './questionlist.css';




const QuestionsList = ({ chapter }) => {

    const id = chapter._id;
    const [updatedChapter, setUpdatedChapter] = useState(chapter);
    const [Checked, setChecked] = useState(chapter.answers);


    const handleChange = (event) => {
        let data = { ...updatedChapter };
        let name = event.target.name;
        let isChecked = event.target.checked;
        data = {
            ...data,
            answers: {
                ...data.answers,
                [name]: isChecked
            }
        }
        setUpdatedChapter(data);
    }
    useEffect(() => {
        let cancel = false;
        const getAnswers = async () => {
            const url = "http://localhost:3000/chapters";
            const res = await axios.get(url)
            const current = res.data.find(chapter => chapter._id === id);
            if (cancel) return;
            setChecked(current.answers);
        }
        getAnswers();

        //for cleanup
        return () => {
            cancel = true;
        }
    }, [id, Checked]);


    useEffect(() => {
        const handleSubmit = async () => {
            await axios.put(`http://localhost:3000/chapters/${id}`, updatedChapter);
        };
        handleSubmit();
    }, [updatedChapter, id]);


    return (
        <Box>
            <Box className="questionList">
                {
                    chapter.questions.map((elem, index) => (
                        <Paper className="question-paper" elevation={3} key={index}>
                            <Box >
                                <Typography
                                    variant="h5"
                                    color={"primary"}
                                >{elem.question}
                                </Typography>
                                <Box className="options">
                                    <FormGroup>
                                        {
                                            elem.options.map((option, index) => (

                                                <FormControlLabel control={<Checkbox onChange={handleChange} checked={Checked[option]} />} key={index} label={option} name={option} />

                                            ))
                                        }
                                    </FormGroup>
                                </Box>
                            </Box>
                        </Paper>
                    ))
                }

            </Box>
        </Box>
    );
};

export default QuestionsList;