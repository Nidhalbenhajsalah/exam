import React, { useState } from "react";
import Box from '@mui/material/Box';
import { Typography } from "@mui/material";
import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import QuestionList from './questionslist'
import './singleChapter.css'

const SingleChapter = ({ chapter, showQuestion }) => {



    return (
        <Accordion>
            <AccordionSummary
                elevation={3}
                expandIcon={<ExpandMoreIcon />}
            >
                <Typography
                    variant="h6"
                    className="domaines"
                >{chapter.title}</Typography>

            </AccordionSummary>
            {showQuestion ?

                <Box className="questions-container">
                    <QuestionList className='questionList' chapter={chapter} />
                </Box>
                :
                <Box>
                    <Typography variant="h6" className="domaines">Temps écoulé pour ce module</Typography>
                </Box>}
        </Accordion>
    );
}

export default SingleChapter;