import React, { useState } from "react";
import { Typography } from "@mui/material";
import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import AccordionDetails from '@mui/material/AccordionDetails';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import SingleChapter from "./singleChapter";
import Button from '@mui/material/Button';
import Timer from './timer'


const SingleDomain = ({ domain, chapters }) => {

    const [startTimer, setStartTimer] = useState(false);
    const [showQuestion, setShowQuestion] = useState(true);

    const handleClickIcon = () => {
        setStartTimer(true);
    }

    const whenTimeIsOver = () => {
        setShowQuestion(false);
    }


    return (
        <Accordion>
            <AccordionSummary
                elevation={3}
                expandIcon={<ExpandMoreIcon />}
                onClick={handleClickIcon}
            >
                <Typography variant='h5' className='domaines'>{domain}
                    {startTimer ?
                        <Timer whenTimeIsOver={whenTimeIsOver} />
                        :
                        null
                    }
                    {/* <Button id="submit">Soumettre</Button> */}
                </Typography>

            </AccordionSummary>
            <AccordionDetails>

                {chapters.filter(chapter => chapter.domain === domain).map(chapter => (
                    <SingleChapter key={chapter._id} chapter={chapter} chapters={chapters} showQuestion={showQuestion} />
                ))}
            </AccordionDetails>

        </Accordion>
    );
}
export default SingleDomain;