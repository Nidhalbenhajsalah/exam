const express = require('express');
const cors = require('cors');
const dotenv = require('dotenv');
const db = require('./models');
const questionRoute = require('./routes/question.route');


dotenv.config();
const app = express();
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));


db.mongoose
    .connect(db.url, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    })
    .then(() => {
        console.log('MongoDB Connected...');
    })
    .catch(err => {
        console.log('MongoDB Connection Error: ', err);
        process.exit();
    });



app.use('/', questionRoute);

app.listen(process.env.PORT || 5000, () => {
    console.log(`Server is running on port ${process.env.PORT}`);
})
