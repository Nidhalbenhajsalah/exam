const mongoose = require('mongoose');
const chapterSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    domain: {
        type: String,
        required: true
    },
    questions: {
        type: Array,
        required: true
    },
    answers: {
        type: Object,
        required: true
    }
});

const Chapitre = mongoose.model('Chapitre', chapterSchema);
module.exports = Chapitre;