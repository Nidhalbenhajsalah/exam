const Chapter = require('../models/chapterSchema.js');

//retrieve all chapters
exports.findAll = (req, res) => {
    Chapter.find()
        .then(chapters => {
            res.send(chapters);
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving chapters."
            });
        });
}

//submit answers
exports.submitAnswers = (req, res) => {
    const id = req.params.id;
    Chapter.findByIdAndUpdate(id, req.body)
        .then(chapter => {
            if (!chapter) {
                return res.status(404).send({
                    message: "Chapter not found with id " + id
                });
            }
            res.send(chapter);
        }
        ).catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving chapter."
            });
        });
};