
const router = require('express').Router();
const chapterController = require('../controllers/chapter.controller');


router.route('/chapters').get(chapterController.findAll);
router.route('/chapters/:id').put(chapterController.submitAnswers);

module.exports = router;